module.exports = {
	name: 'Mog Plush',

	init: function () {
        // TODO: add a command that gives the player the item,
        // instead of giving it to them every time they log in.
        // See #1640
        // this.events.on('onBeforeGetChatCommands' )
        
        this.events.on('onAfterPlayerEnterZone', this.onAfterPlayerEnterZone.bind(this))
    },
    
    // Every time the player enters a zone, give them a mog plush for... testing.
    onAfterPlayerEnterZone: function (obj) {
        obj.inventory.getItem({
            name: 'M\'ogresh Plush',
            quality: 4,
            type: 'toy',
            spritesheet: 'server/mods/mog-plush/images/items.png',
            sprite: [0, 0]
        });
    }
};
