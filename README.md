# M'ogresh Plush Example
This mod is an example of how to add a new image and use it as an item.
For now, it gives you a mog plush whenever you enter a zone, but I'll change it to a command that gives you one when that is improved (#1640).

The image is by EpicDragon.

## Installation
Clone this repository into `src/server/mods` (the folder name that it is cloned into is important for the spritesheet path) and run Isleward:

```console
$ cd src/server/mods
$ git clone https://gitlab.com/Isleward-Modding/mods/mog-plush.git
(cloning into "mog-plush")
$ cd ..
$ node index.js
```

## With /getItem
Get a perfect level 20 epic trinket with the name and sprite with str, str, vit, and 1 random roll.
However, this item will be a random type of trinket with a random implicit (Smokey Orb, Forged Ember, etc).
```
/getItem spritesheet=server/mods/mog-plush/images/items.png sprite=0_0 slot=trinket level=20 quality=3 name=M'ogresh_Plush perfection=1 stats=str,str,vit
```